package config;

import configLoader.ConfigLoader;
import db.DbService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import({
        ConfigLoader.class,
        DbService.class
})
@Configuration
public class PerfConfig {
}
