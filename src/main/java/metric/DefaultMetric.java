package metric;

import com.mongodb.BasicDBObject;
import db.DbService;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.UnknownHostException;

public class DefaultMetric<T> {

    @Autowired
    private DbService dbService;


    String projectName;
    String runName;
    String metricName;
    String nameX;
    String nameY;

    public DefaultMetric(String projectName, String runName, String metricName, String nameX, String nameY) {
        this.projectName = projectName;
        this.runName = runName;
        this.metricName = metricName;
        this.nameX = nameX;
        this.nameY = nameY;
    }

    public void sendData(T key, Float value) {
        BasicDBObject message = generateMessage(key, value);
        try {
            if (!dbService.getDb().collectionExists(projectName)) dbService.getDb().createCollection(projectName, null);
            dbService.getDb().getCollection(projectName).insert(message);
        } catch (UnknownHostException e) {
//            todo: add logging
        }
    }

    private BasicDBObject generateMessage(T key, Float value) {
        return new BasicDBObject("runName", runName)
                .append("metricName", metricName)
                .append(nameX, key.toString())
                .append(nameY, value.toString());
    }
}
