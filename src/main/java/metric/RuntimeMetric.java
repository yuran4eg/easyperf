package metric;

public class RuntimeMetric<T> extends DefaultMetric {

    private int timeoutInSec;

    public RuntimeMetric(String projectName, String runName, String metricName, String nameX, String nameY, int timeoutInSec) {
        super(projectName, runName, metricName, nameX, nameY);
        this.timeoutInSec = timeoutInSec;
    }


//    todo: add methods to collect and send data periodically with timeoutInSec
}
