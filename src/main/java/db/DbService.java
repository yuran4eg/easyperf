package db;

import com.mongodb.DB;
import com.mongodb.MongoClient;
import configLoader.ConfigLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.net.UnknownHostException;

@Service
public class DbService {

    private final ConfigLoader config;

    private static MongoClient client = null;

    @Autowired
    public DbService(ConfigLoader config) {
        this.config = config;
    }

    public DB getDb() throws UnknownHostException {
        if (client == null) {
//            todo: use config
            client = new MongoClient("localhost",27017);
        }
        return client.getDB("configLoader");
    }

    @PreDestroy
    public void cleanUp() {
        client.close();
    }
}
