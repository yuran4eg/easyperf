import config.PerfConfig;
import metric.DefaultMetric;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = PerfConfig.class)
public class PerfTest {
    @Test
    public void test1() {
        DefaultMetric<LocalDate> metric = new DefaultMetric<>("TestProject", "TestRun","testMetric", "Time", "Counter");
        metric.sendData(LocalDate.now(), 100f);

    }
}
